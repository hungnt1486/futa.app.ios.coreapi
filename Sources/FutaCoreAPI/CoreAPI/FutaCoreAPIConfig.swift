//
//  FutaCoreAPIConfig.swift
//  FutaCoreAPI
//
//  Created by mac on 15/08/2022.
//

import Foundation
import RxSwift

@objcMembers
public class FutaCoreAPIConfig: NSObject {
    public static func isLanguageVN() -> Bool {
        let locale = UserDefaults.standard.string(forKey: "app_lang")
        return locale?.elementsEqual("vi") ?? false
    }
    
    public static var appVersion: String = ""
    
    public static var appId: String = ""
    
    public static var userToken: String = ""
    
    public static var userID: String = ""
    
    public static var isProductMode = true
    
    public static var geoHash = BehaviorSubject<String?>(value: "w3gv5zgy")
    
    public static var currentUserLocation = BehaviorSubject<(lng: Double, lat: Double)?>(value: (lng: 10.76634, lat: 106.69355))
}
