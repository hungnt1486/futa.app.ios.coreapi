//
//  Locale+Extension.swift
//  FutaCoreAPI
//
//  Created by mac on 23/08/2022.
//

import Foundation

extension Locale {
    static var backendDefault: Locale {
        return Locale(identifier: "en")
    }
    
    static var supportedLanguages: [String] {
       // return Bundle.getArrayInfoFor(key: AppEnvironment.infoPlist.supportedLanguages.rawValue)
        return ["vi", "en"]
    }
    
    var shortIdentifier: String {
        return String(identifier.prefix(2))
    }
}

extension String {
    
    /* Localize with default device locale */
    func localized(_ locale: String = Locale.preferredLanguages[0]) -> String {
        var localeId = String(locale.prefix(2))
        if !Locale.supportedLanguages.contains(localeId) {
            localeId = Locale.backendDefault.shortIdentifier
        }

//        guard let path = Bundle.main.path(forResource: localeId, ofType: "lproj"),
//              let bundle = Bundle(path: path) else {
//            return NSLocalizedString(self, comment: "")
//        }
        guard let bundle = Bundle(identifier: "FutaCoreAPI1") else {
            return NSLocalizedString(self, comment: "")
        }

        return bundle.localizedString(forKey: self, value: nil, table: nil)
    }
}


