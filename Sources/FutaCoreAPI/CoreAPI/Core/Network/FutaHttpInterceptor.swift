//
//  HttpInterceptor.swift
//  FUTA
//
//  Created by mac on 19/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

public class FutaHttpInterceptor: RequestInterceptor {
    public let retryLimit = 2
    public let retryDelay: TimeInterval = 10
    public var isPersonalAccount = true
    public var ignoreRetry = false
    public var cancellationBag = DisposeBag()
    
    public init() {}
    
    public func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        
//        var urlRequest = urlRequest
//        let accessToken = isPersonalAccount ? AccountHelper.getAccessToken() : AccountHelper.getOrgAccessToken()
//        if let accessToken = accessToken {
//            urlRequest.headers.add(HTTPHeader.authorization(bearerToken: accessToken))
//        }
        completion(.success(urlRequest))
         
    }
    
    public func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        let response = request.task?.response as? HTTPURLResponse
        if let statusCode = response?.statusCode,
           statusCode == 401,
           request.retryCount < retryLimit, !ignoreRetry {
            refreshToken(completion: completion)
        } else {
            return completion(.doNotRetry)
        }
    }
    
    private func refreshToken(completion: @escaping (RetryResult) -> Void) {
        /*
        let authRepo = AuthenticationRepositoryImpl(authService: AuthServiceImpl())
        let refreshToken = isPersonalAccount ? authRepo.refreshToken() : authRepo.refreshOrgToken()
        refreshToken.subscribe(on: DispatchQueue.global()).sinkToResult { (result) in
            switch result {
            case .success:
                return completion(.retry)
            default:
                return completion(.doNotRetry)
            }
        }.store(in: cancellationBag)
         */
    }
}
