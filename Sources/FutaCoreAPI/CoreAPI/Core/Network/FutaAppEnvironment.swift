//
//  AppEnvironment.swift
//  FUTA
//
//  Created by mac on 19/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import Foundation

public struct FutaAppEnvironment {
    public enum FutaDomain: CustomStringConvertible {
        
        case vato
        case futabus
        case busline
        case qrCodeLink
        case mapVato
        case futaExpress
        
        public var description: String {
            switch self {
            case .mapVato:
                if FutaCoreAPIConfig.isProductMode {
                    return "https://map.vato.vn/api"
                }
                return "https://map-dev.vato.vn/api"
            case .vato:
                if FutaCoreAPIConfig.isProductMode {
                    return "https://api.vato.vn/api"
                }
                return "https://api-dev.vato.vn/api"
            case .futabus:
                if FutaCoreAPIConfig.isProductMode {
                    return "https://api.futabus.vn"
                }
                return "https://api-dev.futabus.vn"
            case .busline:
                if FutaCoreAPIConfig.isProductMode {
                    return "https://api-busline.vato.vn/api"
                }
                return "https://api-busline-dev.vato.vn/api"
            case .qrCodeLink:
                if FutaCoreAPIConfig.isProductMode {
                    return "https://futabus.vn/api"
                }
                return "https://dev.futabus.vn/api"
            case .futaExpress:
                if FutaCoreAPIConfig.isProductMode {
                    return "https://api.futaexpress.vn"
                }
                return "https://api-dev.futaexpress.vn"
            }
        }
    }
}
