//
//  FutaAPICall.swift
//  FutaCoreAPI
//
//  Created by Vu Mai Hoang Hai Hung on 06/08/2022.
//


import Foundation
import Alamofire
import RxSwift

public protocol FutaAPICall {
    var domain: FutaAppEnvironment.FutaDomain { get }
    var path: String { get }
    var method: Alamofire.HTTPMethod { get }
    var paramEncoding: ParameterEncoding { get }
    var headers: HTTPHeaders { get }
    var urlParams: [String: Any]? { get }
    var interceptor: RequestInterceptor { get }
}

extension FutaAPICall {
    var baseUrl: String {
        domain.description + path
    }
    
    var paramEncoding: ParameterEncoding {
        URLEncoding(destination: .queryString)
    }
    
    var urlParams: [String: Any]? {
        [String: Any]()
    }
    
    var interceptor: RequestInterceptor {
        FutaHttpInterceptor()
    }
    
    public var baseHeader: HTTPHeaders {
        let headers: HTTPHeaders = [
            "x-platform": "ios",
            "x-app-id": FutaCoreAPIConfig.appId,
            "x-app-version": FutaCoreAPIConfig.appVersion,
            "Content-Type" : "application/json",
            "Accept-Language" :  FutaCoreAPIConfig.isLanguageVN() ? "vi" : "en"
        ]
        return headers
    }
}

public enum FutaAPIError: Swift.Error {
    case invalidURL
    case serviceReponseError(FutaErrorInfo)
    case unexpectedResponse
    case decodeError
    case apiReponseError(FutaErrorInfo)
    case custom(String)
    case networkDown
}

extension FutaAPIError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .invalidURL: return "Invalid URL"
        case .serviceReponseError(let errorInfo): return "Unexpected HTTP code: \(errorInfo.status)"
        case .unexpectedResponse: return "There was an error, please try again!"//"unexpected_error".localized()
        case .decodeError: return "Decode Error"
        case .apiReponseError(let errorInfo): return errorInfo.message
        case .custom(let errString): return errString
        case .networkDown: return "Network connection is disconnected. Please check and try again."//"network_down".localized()
        }
    }
    var errorType: FutaAPIError {
        return self
    }
    
    public var errorCode: String? {
        switch self {
        case .serviceReponseError(let errorInfo): return errorInfo.errorCode
        case .apiReponseError(let errorInfo): return errorInfo.errorCode
        default:
            return nil
        }
    }
}

typealias HTTPCode = Int

enum HttpRequestHeaders {
    enum ContentType {
        static let formUrlEncoded = "application/x-www-form-urlencoded"
        static let json = "application/json"
        static let patchJson = "application/json-patch+json"
    }
}

struct EmptyEntity: Codable, EmptyResponse {
    static func emptyValue() -> EmptyEntity {
        return EmptyEntity.init()
    }
}

extension FutaAPICall {
    public func call() -> RxSwift.Observable<Void> {
        return RxSwift.Observable<Void>.create { observer in
            let request = AF.request(self.baseUrl,
                                     method: method,
                                     parameters: urlParams,
                                     encoding: paramEncoding,
                                     headers: headers
                                     //interceptor: interceptor
            )
            request.validate()
                .response { (response) in
                    
                    switch response.result {
                    case .success(let value):
                        guard let value = value else {
                            logAPIError(result: FutaAPIError.unexpectedResponse.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
                            observer.onError(FutaAPIError.unexpectedResponse)
                            return
                        }
                        logAPISuccess(result: (String(data: value, encoding: .utf8) ?? ""), path: baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())

                        
                        resultHandlder(data: value, observer: observer)
                    case .failure(let error):
                        logAPIError(result: error.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
                        errorHandler(response: response, error: error, observer: observer)
                    }
                }
            return Disposables.create() {
                request.cancel()
            }
            
        }
    }
    
    
    public func call<T: Decodable>() -> RxSwift.Observable<T> {
        return RxSwift.Observable<T>.create { observer in
            
            let request = AF.request(self.baseUrl,
                                     method: method,
                                     parameters: urlParams,
                                     encoding: paramEncoding,
                                     headers: headers
                                     //interceptor: interceptor
            )
            request.validate()
                .response { (response) in
                    
                    switch response.result {
                    case .success(let value):
                        guard let value = value else {
                            logAPIError(result: FutaAPIError.unexpectedResponse.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
                            observer.onError(FutaAPIError.unexpectedResponse)
                            return
                        }
                        logAPISuccess(result: (String(data: value, encoding: .utf8) ?? ""), path: baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
                        
                        resultHandlder(data: value, observer: observer)
                    case .failure(let error):
                        logAPIError(result: error.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
                        errorHandler(response: response, error: error, observer: observer)
                    }
                }
            return Disposables.create() {
                request.cancel()
            }
        }
    }
    
//    func calls<T: Decodable>() -> Observable<[T]> {
//        return Observable<[T]>.create { observer in
//
//            let request = AF.request(self.baseUrl,
//                                     method: method,
//                                     parameters: urlParams,
//                                     encoding: paramEncoding,
//                                     headers: headers
//                                     //interceptor: interceptor
//            )
//            request.validate()
//                .response { (response) in
//
//                    switch response.result {
//                    case .success(let value):
//                        guard let value = value else {
//                            logAPIError(result: APIError.unexpectedResponse.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
//                            observer.onError(APIError.unexpectedResponse)
//                            return
//                        }
//                        logAPISuccess(result: (String(data: value, encoding: .utf8) ?? ""), path: baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
//
//                        resultHandlder(data: value, observer: observer)
//                    case .failure(let error):
//                        logAPIError(result: error.localizedDescription, path: self.baseUrl, method: method, params: urlParams, header: headers, cURL: request.cURLDescription())
//                        errorHandler(response: response, error: error, observer: observer)
//                    }
//                }
//            return Disposables.create() {
//                request.cancel()
//            }
//        }
//    }
//
    private func resultHandlder(data: Data, observer: AnyObserver<Void>) {
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            if let status = json?["status"] as? Int, status != 200 {
                observer.onError(FutaAPIError.apiReponseError(FutaErrorInfo(data: data, statusCode: status)))
                return
            }
            
            observer.on(.next(()))
            observer.on(.completed)
            
        } catch {
            if !FutaCoreAPIConfig.isProductMode {
                debugPrint("== hung decode error: \(error)")
                observer.onError(FutaAPIError.decodeError)
            } else {
                observer.onError(FutaAPIError.unexpectedResponse)
            }
        }
    }
    
    private func resultHandlder<T: Decodable>(data: Data, observer: AnyObserver<T>) {
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
            if let status = json?["status"] as? Int, status != 200 {
                observer.onError(FutaAPIError.apiReponseError(FutaErrorInfo(data: data, statusCode: status)))
                return
            }
            
            let responseData = try JSONDecoder().decode(WrapResponse<T>.self, from: data)
            let status = responseData.status
            if status != 200 {
                if let message = responseData.message {
                    observer.onError(FutaAPIError.custom(message))
                } else {
                    observer.onError(FutaAPIError.unexpectedResponse)
                }
            } else {
                let data = responseData.data
                observer.on(.next(data))
                observer.on(.completed)
            }
        } catch {
            if !FutaCoreAPIConfig.isProductMode {
                debugPrint("== hung decode error: \(error)")
                observer.onError(FutaAPIError.decodeError)
            } else {
                observer.onError(FutaAPIError.unexpectedResponse)
            }
        }
    }
    
    private func errorHandler<Output>(response: AFDataResponse<Data?>, error: AFError, observer: AnyObserver<Output>) {

        if let code = (error.underlyingError as? URLError)?.errorCode,
           code == NSURLErrorNotConnectedToInternet || code == NSURLErrorBadServerResponse {
            observer.on(.error(FutaAPIError.networkDown))
            return
        }
        guard let code = error.responseCode else {
            observer.on(.error(FutaAPIError.unexpectedResponse))
            return
        }
        
        observer.onError(FutaAPIError.apiReponseError(FutaErrorInfo(data: response.data, statusCode: code)))
    }
    
    private func logAPISuccess(result: String,path: String, method: Alamofire.HTTPMethod, params: Parameters?, header: HTTPHeaders?, cURL: String) {
        
        if !FutaCoreAPIConfig.isProductMode {
            
            let text = """
                        ======================
                        ℹ️ Input:
                        \(geDescription(path: path, method: method, params: params, header: headers))
                        
                        🔥 Response:
                        \(result)
                        ======================
                        
                        ⚠️ CURL:
                        \(cURL)
                        """
            print(text)
        }
        
        
//        NSLog("ℹ️ Input:")
//        NSLog("\(geDescription(path: path, method: method, params: params, header: headers))")
//        NSLog("🔥 Response:")
//        NSLog("\(result)")
//        NSLog("⚠️ CURL::")
//        NSLog("\(cURL)")
       
    }
    
    private func logAPIError(result: String,path: String, method: Alamofire.HTTPMethod, params: Parameters?, header: HTTPHeaders?, cURL: String) {
   
        if !FutaCoreAPIConfig.isProductMode {
            let text = """
                           ======================
                           ℹ️ Input:
                           \(geDescription(path: path, method: method, params: params, header: headers))
                           
                           🐞 Error::
                           \(result)
                           ======================
                           
                           ⚠️ CURL:
                           \(cURL )
                           """
            print(text)
            
        }
        
//        NSLog("ℹ️ Input:")
//        NSLog("\(geDescription(path: path, method: method, params: params, header: headers))")
//        NSLog("🔥 Response:")
//        NSLog("\(result)")
//        NSLog("⚠️ CURL::")
//        NSLog("\(cURL)")
        
    }
    
    
    private func geDescription(path: String, method: Alamofire.HTTPMethod, params: Parameters?, header: HTTPHeaders? ) -> String {
        return """
                   path: \(path)
                   method: \(method)
                   params: \(params ?? [:])
                   header: \(header ?? [:])
               """
    }
}

