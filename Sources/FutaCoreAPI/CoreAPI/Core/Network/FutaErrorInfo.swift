//
//  ErrorInfo.swift
//  FUTA
//
//  Created by mac on 19/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import Foundation
import RxSwift
import Action

public class FutaErrorInfo: Error ,Decodable {
    var status: Int = 0
    var message: String = ""
    var errorCode: String?
    
    enum CodingKeys: String, CodingKey {
        case message
        case status
        case errorCode
    }
    
    init(data: Data?, statusCode: Int) {
        self.status = statusCode
        guard let data = data,
              let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
            return
        }
        if let errorDescription = json["message"] as? String {
            self.message = errorDescription
        }
        
        if let errorCode = json["errorCode"] as? String {
            self.errorCode = errorCode
        }
    }
    
    init(statusCode: Int, message: String, errorCode: String?) {
        self.status = statusCode
        self.message = message
        self.errorCode = errorCode
    }
}

public extension ObservableType where Element == ActionError {
    
    var apiError: Observable<FutaAPIError> {
        return flatMap { actionError -> Observable<FutaAPIError> in
            switch actionError {
            case .underlyingError(let apiError as FutaAPIError):
                return .just(apiError)
            default:
                return .empty()
            }
        }
    }
}
