//
//  LinkSHBAccountType.swift
//  FutaCoreAPI
//
//  Created by mac on 28/09/2022.
//

public enum LinkSHBAccountType: String {
    case card = "CARD"
    case account = "ACCOUNT"
}
