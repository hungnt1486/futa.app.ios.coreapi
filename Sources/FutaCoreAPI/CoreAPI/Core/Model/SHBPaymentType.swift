//
//  SHBPaymentType.swift
//  FutaPay
//
//  Created by mac on 03/10/2022.
//

public enum SHBPaymentType: String {
    case recharge = "TOP_UP" // Nap tien tu SHB vao FutaPay
    case refund = "REFUND" // Hoan tien ve the/tai khoan SHB
    case linkAccount = "LINK_ACCOUNT" // Link the/tai khoan SHB
    case payment = "PAYMENT" // Thanh toan truc tiep bang the/tai khoan SHB
}

