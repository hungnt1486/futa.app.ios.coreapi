//
//  CityBusPaymentType.swift
//  FutaCoreAPI
//
//  Created by MACOS on 22/12/2022.
//

public enum CityBusOrderType: Int {
    case registryTicket = 1 // Dang ky ve moi
    case extendTicket = 2 // Gia hang ve
}
